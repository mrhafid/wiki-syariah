import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:path_provider/path_provider.dart';


class Manager {
  String api = 'http://klepon.tech/projects/wikisyariah/api';
//  http://klepon.tech/projects/wikisyariah/api
  String title = 'wiki_syariah';
  Manager._internal();
  static final Manager instance = new Manager._internal();

  void setApi (String _api){
    writeDataApi(_api);
    api = _api;
  }

  void resetApi () {
//    _api = null;
    deleteApi();
  }

  Future<File> get _localFile async {
    String saveApi = (await getExternalStorageDirectory()).path;
    return File('$saveApi/$title.txt');
  }

  Future<File> writeDataApi(String api) async {
    final file = await _localFile;
    file.writeAsStringSync('');
    return file.writeAsString('$api');
  }

  Future<void> deleteApi() async {
    final file = await _localFile;
    file.writeAsStringSync('');
  }

  Future<String> readApi() async {
    try {
      final file = await _localFile;
      // Read the file.
      String dataApi = await file.readAsString();
      if (file == null) {
        resetApi();
        return '';
      } else {
        setApi(dataApi);
        print(api);
        return dataApi;
      }
    } catch (e) {
      return '';
    }
  }

    get connection async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
  }
}
