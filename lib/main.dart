import 'dart:async';
import 'dart:convert';

import 'package:ahliwaris_android/manager/manager.dart';
import 'package:ahliwaris_android/services/data.dart';
import 'package:ahliwaris_android/models/data.dart' as modelData;
import 'package:ahliwaris_android/views/setting.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() async {
  var isConnected = await Manager.instance.connection;
  if (isConnected) {
    var api = await Manager.instance.readApi();
//    debugPrint(api);
    if (api != '') {
      Manager.instance.api = api;
    }
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wiki Syariah',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: splashScreen('Wiki Syariah'),
      ),
//        MyHomePage(title: 'Wiki Syariah')
      routes: <String, WidgetBuilder>{
        '/setting': (BuildContext context) => new SettingPage(),
      },
    );
  }

  Widget splashScreen(String title) {
    return SplashScreen(title: title);
  }
}

class SplashScreen extends StatefulWidget {
  final String title;
  SplashScreen({Key key, this.title}) : super(key: key);

  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MyHomePage(
            title: widget.title,
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(20.0),
      alignment: Alignment.center,
      height: MediaQuery.of(context).size.height,
      child: Image(
        width: 100,
        image: AssetImage('assets/images/logo.jpg'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<modelData.Data> data;
  List<modelData.Data> filterData;

  @override
  void initState() {
    super.initState();
    getData();
  }

  void _showDialog(String pesan) {
    // flutter defined function
    showGeneralDialog(
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(
              opacity: a1.value,
              child: CupertinoAlertDialog(
                title: Text('Peringatan!'),
                content: Text(pesan),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  CupertinoDialogAction(
                    child: Text("Oke"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ),
            ),
          );
        },
        transitionDuration: Duration(milliseconds: 200),
        barrierDismissible: true,
        barrierLabel: '',
        context: context,
        pageBuilder: (context, animation1, animation2) {
          return null;
        });
  }

  void getData() async {
    await http.get(Manager.instance.api + '/waris/').then((res) async {
      data = await json
          .decode(res.body)
          .map((d) => modelData.Data(d))
          .toList()
          .cast<modelData.Data>();
      setState(() => filterData = data);
    }).catchError((e) {
      _showDialog(' Url Server Not Specified');
    });
  }

  void filter_data(String text) {
    if (text.length > 1) {
      setState(() {
        filterData = data.where((d) {
          if (d.judul.toLowerCase().indexOf(text) > -1) {
            return true;
          } else if (d.deskripsi.toLowerCase().indexOf(text) > -1) {
            return true;
          }
          return false;
        }).toList();
      });
    } else {
      setState(() {
        filterData = data;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.refresh,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
//                  main();
//                  print(Manager.instance.api);
//                  filterData = null;
                  getData();
                });
              },
            ),
            IconButton(
              icon: Icon(
                Icons.settings,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SettingPage(),
                  ),
                );
              },
            ),
          ],
        ),
        body: Container(
          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
          color: Color.fromRGBO(222, 232, 243, 3.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
//                    color: Color.fromRGBO(191, 195, 208, 3.0),
//                    padding: EdgeInsets.only(top: 20.0),
                child: TextField(
                    style: TextStyle(
                      fontSize: 20.0,
//                        color: Color.fromRGBO(188, 192, 205, 3.0),
                    ),
                    onChanged: (value) {
                      if (value != '') {
                        filter_data(value);
                      }
                    },
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        prefixIcon: Icon(Icons.search),
                        hintText: "Search...",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(191, 195, 208, 3.0),
                                width: 28.0),
                            borderRadius: BorderRadius.circular(25.0)))),
              ),
              Expanded(
                  child: filterData != null
                      ? _listData()
                      : Container(
                          alignment: Alignment.center,
                          child: CircularProgressIndicator(),
                        ))
            ],
          ),
        )
        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

  Widget _listData() {
    return ListView.builder(
      itemCount: filterData.length * 2,
      itemBuilder: (context, i) {
        if (i.isOdd) return Divider();

        var index = i ~/ 2;
        return ListTile(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                filterData[index].judul,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                filterData[index].deskripsi,
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
            ],
          ),
        );
      },
    );
  }
}
