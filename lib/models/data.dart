class Data {
  int id;
  String judul;
  String deskripsi;

  Data(json) :
      id = json['id'],
      judul = json['word'],
      deskripsi = json['description'];
}