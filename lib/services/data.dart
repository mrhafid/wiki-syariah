
import 'package:ahliwaris_android/manager/manager.dart';
import 'network_service.dart';

class Data {
  static String baseUrl = Manager.instance.api+'/waris';

  static Map endpoints = {
    'index': {'url': '$baseUrl/', 'type': Method.get},
    'create': {'url': '$baseUrl/create', 'type': Method.post},
    'show': {'url': '$baseUrl/show/{}', 'type': Method.get},
    'update': {'url': '$baseUrl/update/{}', 'type': Method.post},
    'delete': {'url': '$baseUrl/delete/{}', 'type': Method.delete},


  };

  static Future<ResponseMessage> send(String key, {List<String> params, Map body}) {
    Map data = endpoints[key];
    return NetworkService.send(data['type'], data['url'], body, params);
  }
}
