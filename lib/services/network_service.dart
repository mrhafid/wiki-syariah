import 'dart:io';
import 'dart:async';
import 'dart:convert';
import 'package:ahliwaris_android/manager/manager.dart';
import 'package:http/http.dart' as http;

class ResponseMessage {
  final dynamic body;
  final int statusCode;
  final bool isSuccess;

  ResponseMessage({
    this.body,
    this.statusCode,
    this.isSuccess = true
  });
}

enum Method {
  post,
  get,
  put,
  patch,
  delete
}

class NetworkService {


  static Future<ResponseMessage> send(Method type, String url, [Map body, List<String> params]) async {
    if (params != null) {
      params.forEach((p) {
        var index = url.indexOf('{}');
        if (index > -1) {
          url = '${url.substring(0, index)}$p${url.substring(index+2, url.length)}';
        }
      });
    }
    print(url);
    NetworkService service = new NetworkService();
    switch(type) {
      case Method.get:
        return service.get(url);
      case Method.post:
        return service.post(url, body);
      case Method.put:
        return service.put(url, body);
      case Method.patch:
        return service.patch(url, body);
      case Method.delete:
        return service.delete(url);
      default:
        return ResponseMessage(body: {'message': 'Unknown Method'}, statusCode: 405, isSuccess: false);
    }
  }

  Future<ResponseMessage> post(String url, Map body)
  {
    return http.post(url,
        body: body,
    )
        .then((http.Response res) {
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        error(res, statusCode);
      }
      print(res.body.toString());
      return ResponseMessage(body: json.decode(res.body), statusCode: statusCode);
    });
  }

  Future<ResponseMessage> get(String url)
  {
//    print(url);
    return http.get(url)
        .then((http.Response res) {
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        error(res, statusCode);
      }

      return ResponseMessage(body: json.decode(res.body), statusCode: statusCode);
    });
  }

  Future<ResponseMessage> put(String url, Map body)
  {
    return http.put(url,
      body: body,
    )
        .then((http.Response res) {
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        error(res, statusCode);
      }

      return ResponseMessage(body: json.decode(res.body), statusCode: statusCode);
    });
  }

  Future<ResponseMessage> patch(String url, Map body)
  {
    return http.patch(url,
      body: body,
    )
        .then((http.Response res) {
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        error(res, statusCode);
      }

      return ResponseMessage(body: json.decode(res.body), statusCode: statusCode);
    });
  }

  Future<ResponseMessage> delete(String url)
  {
    return http.delete(url,
    )
        .then((http.Response res) {
      final int statusCode = res.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        error(res, statusCode);
      }

      return ResponseMessage(body: json.decode(res.body), statusCode: statusCode);
    });
  }
  void error(res, statusCode) {
        throw new Exception(ResponseMessage(body: json.decode(res.body), statusCode: statusCode, isSuccess: false));

    }



}
