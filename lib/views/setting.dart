import 'package:ahliwaris_android/manager/manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

class SettingPage extends StatefulWidget {
  @override
  SettingPageState createState() => SettingPageState();
}

class SettingPageState extends State<SettingPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController urlController = new TextEditingController();

  @override
  void initState() {
    super.initState();

      urlController.text = Manager.instance.api;

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
        appBar: AppBar(
          title: Text('Update Url Server'),
          elevation: 0.0,
        ),
        body: Stack(alignment: Alignment.topCenter, children: <Widget>[
          SingleChildScrollView(
            child: Column(children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 10.0, right: 10.0),
                child: Form(
                  key: _formKey,
                  child: TextFormField(
                    controller: urlController,
                    decoration: InputDecoration(labelText: 'Url Server'),
                    style: TextStyle(color: Colors.grey),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter new url server';
                      }
                      return null;
                    },
                  ),
                ),
              ),
              Container(
//                width: MediaQuery.of(context).size.width,
                height: 50.0,
                padding: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
                margin: EdgeInsets.only(top: 20.0),
                alignment: Alignment.centerRight,
                child: RaisedButton(
                  onPressed: urlController.text == Manager.instance.api  ? null : () {
                    if (_formKey.currentState.validate()) {
                      // If the form is valid, display a Snackbar.
                      Manager.instance.setApi(urlController.text);
                      setState(() {
                        urlController.text = Manager.instance.api;
                      });

                    }
                  },
                  color: Colors.blue,
                  child: Stack(
                    alignment: Alignment.bottomRight,
                    children: <Widget>[
                      Text(
                        'update'.toUpperCase(),
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0)),
                ),
              ),
            ]),
          ),
          Container(),
        ]));
  }
}
